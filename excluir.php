<?php
    include 'header/index.php';//carregando dados head com os estilos
    include 'header/menu.php';//carregando dados menu
    include 'conexao/conexao.php';
    if(mysqli_connect_errno()){
        die("Conexao falhou" . mysqli_connect_errno());
    }
    
?>
<?php //excluindo do  banco
    if(isset($_POST['nome'])){
        $empresaId = $_POST['id'];
        $deletar = "UPDATE empresa SET status_id =2  WHERE id = {$empresaId} ";
        $excluindo = mysqli_query($conecta,$deletar);
        if(!$excluindo){
            die("Não é possivel excluir");
        }else{
            header("location:listagem.php");
        }
    }


    //consulta a tabela empresa
    $recuperar ="SELECT * FROM empresa  ";
    if(isset($_GET['codigo'])){
        $id = $_GET['codigo'];
        $recuperar .= " WHERE id = {$id} ";
    } 
    $atualizar = mysqli_query($conecta,$recuperar);
    if(!$atualizar){
        die("Erro na consulta");
    }
    $dados = mysqli_fetch_assoc($atualizar);
?>

<h1 class="text-center">Excluir dados da empresa</h1>
<div class="container">
    <div class="row">
        <div class="col-12 mb-3">
            <form method="post">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Contato:</label>
                        <input type="text" class="form-control" id="inputEmail4" name="nome"  placeholder="Contato" value="<?php echo utf8_encode($dados["nome"]) ?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4"> E-mail</label>
                        <input type="text" class="form-control" id="inputPassword4" name="email"  placeholder="E-mail" value="<?php echo utf8_encode($dados["email"]) ?>"> 
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Telefone:</label>
                        <input type="text" class="form-control" id="telefone" name="celular"  placeholder="Telefone " value="<?php echo utf8_encode($dados["telefone"]) ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4"> Empresa:</label>
                        <input type="text" class="form-control" id="inputPassword4" name="empresa"  placeholder="Empresa" value="<?php echo utf8_encode($dados["empresanome"]) ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4"> CNPJ:</label>
                        <input type="text" class="form-control" id="cnpj" name="cnpj"  placeholder="CNPJ" value="<?php echo utf8_encode($dados["cnpj"]) ?>">
                     </div>
                </div>
                    <input type="hidden" name="id" value="<?php echo $dados["id"] ?>">
                <button type="submit" id="excluir"  class="btn btn-primary">Deletar</button>
            </form>
        </div>
    </div>
</div>
<?php
    include 'footer/index.php';

?>