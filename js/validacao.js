$(function(){
    
    $('#cep').blur(function(e) {//usando api cep e fazendo validacao
        var cep = $('#cep').val();
        var url = "http://viacep.com.br/ws/" + cep + "/json/";
        var validacep =/^[0-9]{5}-?[0-9]{3}$/;
        if(validacep.test(cep)){
            pesquisaCep(url);
            $('#msg').hide();
        } else {
            $('#msg').addClass('d-block');
            $('#msg p').html("CEP invalido!");
        }
        
    });
    function pesquisaCep(endereco){//criando funcao ajax para inserir nos campos
        $.ajax({
            type:"GET",
            url:endereco,
            async:false
        }).done(function(data){
            
            $('#logradouro').val(data.logradouro);
            $('#bairro').val(data.bairro);
            $('#cidade').val(data.localidade);
            $('#estado').val(data.uf);
        }).fail("Error ao buscar cep");
    };
    //insercao no banco de dados via ajax
    $('#cadastro').submit(function(){
        
        alert("Cadastro com  sucesso!");
        var formulario = $(this);
        var retorno = inserirForm(formulario);
    });
    function inserirForm(dados){
        $.ajax({
            type:"POST",
            data:dados.serialize(),
            url:"./inserir.php",
            async:false
        }).then(sucesso,falha);
        function sucesso(data){
            console.log(data);
        }
        function falha(){

        }
    }
    
})