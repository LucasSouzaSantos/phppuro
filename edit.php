<?php
    include 'header/index.php';
    include 'header/menu.php';
    include 'conexao/conexao.php';
    if(mysqli_connect_errno()){
        die("Conexao falhou" . mysqli_connect_errno());
    }
    
?>
<?php 
 //consulta a tabela empresa
 $recuperar ="SELECT * FROM empresa  ";
 if(isset($_GET['codigo'])){
     $id = $_GET['codigo'];
     $recuperar .= " WHERE id = {$id} ";
 } 
 $atualizar = mysqli_query($conecta,$recuperar);
 if(!$atualizar){
     die("Erro na consulta");
 }
 $dados = mysqli_fetch_assoc($atualizar);
 


 if(isset($_POST['nome'])){//alterando os dados
    $nome = utf8_decode($_POST['nome']);
    $email = $_POST['email'];
    $telefone = $_POST['celular'];
    $empresa = utf8_decode($_POST['empresa']);
    $cnpj = $_POST['cnpj'];
    $cep = $_POST['cep'];
    $logradouro = utf8_decode($_POST['logradouro']);
    $bairro = utf8_decode($_POST['bairro']);
    $numero = utf8_decode($_POST['numero']);
    $cidade = utf8_decode($_POST['cidade']);
    $estado = $_POST['estado'];
    $id =  $_POST['id'];
   
    $alterar = "UPDATE empresa SET  ";
    $alterar .= "nome = '{$nome}',email = '{$email}',telefone = '{$telefone}',empresanome = '{$empresa}',cnpj = '{$cnpj}',cep = '{$cep}',logradouro = '{$logradouro}',bairro = '{$bairro}',numero = '{$numero}',cidade = '{$cidade}',estado = '{$estado}',modified= NOW() ";
    $alterar .= " WHERE id = {$id}";
    
    $operacao = mysqli_query($conecta,$alterar);
    if(!$operacao){
        die("Errou");
    }
    
   
    
}

?>




    <h1 class="text-center">Editar cadastro</h1>
    <div class="container">
        <div class="row">
            <div class="col-12 mb-3">
                <form method="post" action="listagem.php" >
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Contato:</label>
                            <input type="text" class="form-control" id="inputEmail4" name="nome" placeholder="Contato" value="<?php echo utf8_encode($dados["nome"]) ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4"> E-mail</label>
                            <input type="text" class="form-control" id="inputPassword4" name="email" placeholder="E-mail" value="<?php echo $dados["email"] ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">Telefone:</label>
                            <input type="text" class="form-control" id="telefone" name="celular" placeholder="Telefone" value="<?php echo $dados["telefone"] ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4"> Empresa:</label>
                            <input type="text" class="form-control" id="inputPassword4" name="empresa" placeholder="Empresa" value="<?php echo utf8_encode($dados["empresanome"]) ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4"> CNPJ:</label>
                            <input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="CNPJ" value="<?php echo $dados["cnpj"] ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">CEP:</label>
                            <input type="text" class="form-control" id="cep" name="cep" placeholder="CEP" value="<?php echo $dados["cep"] ?>">                        
                        </div>
                    </div>    
                        <div class="alert alert-danger " id="msg" role="alert">
                            <p></p>
                        </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Logradouro:</label>
                            <input type="text" class="form-control" id="logradouro" name="logradouro" placeholder="Logradouro" value="<?php echo utf8_encode($dados["logradouro"]) ?>">                     
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Bairro:</label>
                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value="<?php echo utf8_encode($dados["bairro"])?>">                     
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress">N°/complento:</label>
                            <input type="text" class="form-control" id="numero" name="numero" placeholder="N°" value="<?php echo utf8_encode($dados["numero"]) ?>">                     
                        </div>
                        
                    </div>  
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Cidade:</label>
                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" value="<?php echo utf8_encode($dados["cidade"]) ?>">                     
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Estado:</label>
                            <input type="text" class="form-control" max id="estado" name="estado" placeholder="Estado" value="<?php echo $dados["estado"] ?>">                     
                        </div>
                    </div>
                        <input type="hidden" name="id" value="<?php echo $dados["id"] ?>">
                    <button type="submit" class=" excluir btn btn-primary">Atualizar</button>
                </form>
            </div>
        </div>
    </div>




<?php
    include 'footer/index.php';
    
?>

