-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Ago-2019 às 19:13
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projeto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--
-- Criação: 16-Ago-2019 às 16:16
-- Última actualização: 16-Ago-2019 às 17:01
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefone` varchar(19) DEFAULT NULL,
  `empresanome` varchar(255) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `numero` varchar(60) DEFAULT NULL,
  `cidade` varchar(200) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONAMENTOS PARA TABELAS `empresa`:
--   `status_id`
--       `status` -> `id`
--

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `status_id`, `nome`, `email`, `telefone`, `empresanome`, `cnpj`, `cep`, `logradouro`, `bairro`, `numero`, `cidade`, `estado`, `created`, `modified`) VALUES
(53, 2, 'wqdqw', 'qwdqw@sdasda', '(12)3123-1231', 'weqwe', '12.312.312/3123-12', '09931-070', 'Rua Acácio', 'Campanário', 'e322', 'Diadema', 'SP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 2, 'dqwdq', 'qwqw@asda', '(31)2312-3123', 'apple maça', '12.312.312/3123-12', '09931-416', 'Rua Olivina', 'Tabão', 'qweq', 'Diadema', 'SP', '2019-08-16 13:13:40', '2019-08-16 13:32:46'),
(55, 2, 'asda', 'asda@asda', '(21)3123-1231', 'sdfw', '23.123.123/1231-23', '09931-070', 'Rua Acácio', 'Campanário', 'adqwd', 'Diadema', 'SP', '2019-08-16 13:20:14', '2019-08-16 13:20:14'),
(56, 1, 'asdasdasdas', 'asdasda@asdasd', '(22)3123-1231', 'maÃ§a', '21.312.312/3123-13', '09931-070', 'Rua Acácio', 'Campanário', '55', 'Diadema', 'SP', '2019-08-16 13:42:07', '2019-08-16 13:42:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--
-- Criação: 16-Ago-2019 às 16:15
-- Última actualização: 16-Ago-2019 às 16:18
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONAMENTOS PARA TABELAS `status`:
--

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `nome`, `observacao`) VALUES
(1, 'ativado', 'para elementos ativos'),
(2, 'desativado', 'para elementos que forem desativados\r\n');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ativo/desativado` (`status_id`);

--
-- Índices para tabela `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de tabela `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `ativo/desativado` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
