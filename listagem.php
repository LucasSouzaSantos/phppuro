<?php
    include 'header/index.php';//carregando dados head com os estilos
    include 'header/menu.php';//carregando dados menu
    include 'conexao/conexao.php';
    if(mysqli_connect_errno()){
        die("Conexao falhou" . mysqli_connect_errno());
    }
    include 'dadosform/listagem.php';//carregando dados para listagem

?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <form class="form-inline my-2 my-lg-0" method="get">
                <div class="col-10 col-lg-11">
                    <input class="form-control campo-pesquisa mr-sm-2" type="search" name="procurar" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="col-12">
            <div class="table-responsive tabelas">            
                <table class="table table-dark">
                    <thead>
                        <tr>                        
                            <th scope="col">nome</th>
                            <th scope="col">email</th>
                            <th scope="col">Telefone</th>
                            <th scope="col">Nome fantasia</th>
                            <th scope="col">Cnpj</th>
                            <th scope="col">Criado</th>
                            <th scope="col" class="text-center">Ação</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php while($lista = mysqli_fetch_assoc($consulta)){ ?>
                        <tr>                        
                            <td><?php echo utf8_encode($lista['nome'])?></td>
                            <td><?php echo $lista['email'] ?></td>
                            <td><?php echo $lista['telefone'] ?></td>
                            <td><?php echo utf8_encode($lista['empresanome']) ?></td>
                            <td><?php echo $lista['cnpj'] ?></td>
                            <td><?php echo  date("d/m/Y", strtotime($lista['created']))  ?></td>
                            <td class="d-flex"> 
                                <a href="edit.php?codigo=<?php echo $lista['id'] ?>"> <i class="text-white mr-1  fas fa-eye"></a></i> 
                                <a href="edit.php?codigo=<?php echo $lista['id'] ?>"> <i class="text-white mr-1 fas fa-edit"> </a></i>
                                <a href="excluir.php?codigo=<?php echo $lista['id'] ?>"><i class="text-white mr-1 fas fa-trash-alt"></a></i>
                                </td>
                            
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>            
        </div>
    </div>
</div>





<?php
    include 'footer/index.php';

?>