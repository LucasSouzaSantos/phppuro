
<?php
include 'header/index.php';//carregando dados head com os estilos
include 'header/menu.php';//carregando dados menu
include 'conexao/conexao.php';
if(mysqli_connect_errno()){
    die("Conexao falhou" . mysqli_connect_errno());
}

?>
<h1 class="text-center">Cadastre sua empresa</h1>
<div class="container">
    <div class="row">
        <div class="col-12 mb-3">
            <form id="cadastro" action="listagem.php">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Contato:</label>
                        <input type="text" class="form-control" id="inputEmail4" name="nome" required placeholder="Contato">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4"> E-mail</label>
                        <input type="text" class="form-control" id="inputPassword4" name="email" required placeholder="E-mail">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Telefone:</label>
                        <input type="text" class="form-control" id="telefone" name="celular" required placeholder="Telefone">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4"> Empresa:</label>
                        <input type="text" class="form-control" id="inputPassword4" name="empresa" required placeholder="Empresa">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4"> CNPJ:</label>
                        <input type="text" class="form-control" id="cnpj" name="cnpj" required placeholder="CNPJ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">CEP:</label>
                        <input type="text" class="form-control" id="cep" name="cep" required placeholder="CEP">                        
                    </div>
                </div>    
                    <div class="alert alert-danger " id="msg" role="alert">
                        <p></p>
                    </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Logradouro:</label>
                        <input type="text" class="form-control" id="logradouro" name="logradouro" required placeholder="Logradouro">                     
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Bairro:</label>
                        <input type="text" class="form-control" id="bairro" name="bairro" required placeholder="Bairro">                     
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputAddress">N°/complento:</label>
                        <input type="text" class="form-control" id="numero" name="numero" required placeholder="N°">                     
                    </div>
                    
                </div>  
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputAddress">Cidade:</label>
                        <input type="text" class="form-control" id="cidade" name="cidade" required placeholder="Cidade">                     
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputAddress">Estado:</label>
                        <input type="text" class="form-control" max id="estado" name="estado" required placeholder="Estado">                     
                    </div>
                </div>
                    
                <button type="submit" class="btn btn-primary botao">Cadastrar</button>
            </form>
        </div>
    </div>
</div>




<?php
include 'footer/index.php';

?>

